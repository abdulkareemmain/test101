<?php

use App\Http\Controllers\BuildingController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::resource('buildings', BuildingController::class);
Route::get('buildings/delete/{id}', [App\Http\Controllers\BuildingController::class, 'destroy'])->name('buildings.delete');

Route::get('/exportexcel', [App\Http\Controllers\BuildingController::class, 'export'])->name('exportexcel');

Route::get('/exportpdf', [App\Http\Controllers\BuildingController::class, 'exportpdf'])->name('exportpdf');
