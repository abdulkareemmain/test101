@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                <i class="fas fa-text-width"></i>
                Building {{$building->name}}
            </h3>
        </div>

        <div class="card-body">
            <dl class="row">
                <dt class="col-sm-4">Building Name</dt>
                <dd class="col-sm-8">{{$building->name}}</dd>
                <dt class="col-sm-4">Electricity</dt>
                <dd class="col-sm-8">{{$building->electricity}}</dd>
                <dt class="col-sm-4">Address</dt>
                <dd class="col-sm-8">{{$building->address}}</dd>
                <dt class="col-sm-4">Images</dt>
                <dd class="col-sm-8">
                    @foreach ($building->images as $item)
                    <img class="tableimage" src="/{{$item->media}}" alt="">
                    @endforeach
                </dd>
            </dl>
        </div>

    </div>
@endsection
