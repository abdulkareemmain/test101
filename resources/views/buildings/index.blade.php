@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Buildings</h3>
        </div>

        <div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-right">
                            <a href="{{route('buildings.create')}}"><button class="btn btn-primary" type="button">Create</button></a>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <a href="/exportexcel"><button class="btn btn-primary" type="button">Export To Excel</button></a>
                        <a href="/exportpdf"><button class="btn btn-primary" type="button">Export To PDF</button></a>

                    </div>
                </div>
                <div class="row">

                    <div class="col-sm-12">
                        <table id="buildingstable" class="table table-bordered table-striped dataTable dtr-inline"
                            aria-describedby="example1_info">
                            <thead>
                                <tr>
                                    <th class="sorting sorting_asc" tabindex="0" aria-controls="example1" rowspan="1"
                                        colspan="1" aria-sort="ascending"
                                        aria-label="Rendering engine: activate to sort column descending">Image
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                                        colspan="1" aria-label="Browser: activate to sort column ascending">Name</th>

                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                                        colspan="1" aria-label="Engine version: activate to sort column ascending">Electricity</th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                                        colspan="1" aria-label="CSS grade: activate to sort column ascending">Address
                                    </th>
                                    <th>Actions</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach ($buildings as $item)

                                <tr class="odd">
                                    <td class="dtr-control sorting_1" tabindex="0">
                                        <img class="tableimage" src="{{$item->images->first() ? $item->images->first()->media : ''}}" alt="">
                                    </td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->electricity}}</td>
                                    <td>{{$item->address}}</td>
                                    <td>
                                        <a href="{{route('buildings.show',$item->id)}}"><i class="fas fa-eye"></i></a>

                                        <a href="{{route('buildings.edit',$item->id)}}"><i class="fas fa-edit"></i></a>
                                        <a href="{{route('buildings.delete',$item->id)}}"><i class="fas fa-trash"></i></a>

                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th rowspan="1" colspan="1">Image</th>
                                    <th rowspan="1" colspan="1">Name</th>
                                    <th rowspan="1" colspan="1">Electricity</th>
                                    <th rowspan="1" colspan="1">Address</th>

                                </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-5">
                        <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Showing 1 to
                            {{$buildings->count()}} of {{$buildings->total()}} entries</div>
                    </div>
                    <div class="mt-4">

                        {{$buildings->links('pagination::bootstrap-4')}}
                    </div>

                </div>
            </div>
        </div>

    </div>
@push('page_scripts')
<script>
    $(document).ready(function() {
      $('#example').DataTable({
        dom: 'Bfrtip',
        buttons: [
          'excelHtml5',
          'pdfHtml5'
        ]
      });
    });
  </script>
@endpush
@endsection
