<table>
    <thead>
        <tr>
            <th>Name</th>
            <th>Electricity</th>
            <th>Address</th>
            <th>Image</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($buildings as $item)
            <tr>
                <td>{{ $item->name }}</td>
                <td>{{ $item->electricity }}</td>
                <td>{{ $item->address }}</td>
                <td>

                    @if ($item->images->isNotEmpty())
                    @php
                        $link=url('/').'/'.$item->images->first()->media
                    @endphp
                    <img width=50 height="50" src="{{$link}}" />
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

