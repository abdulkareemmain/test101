<table>
    <thead>
        <tr>
            <th>Name</th>
            <th>Electricity</th>
            <th>Address</th>
            <th>Image</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($buildings as $item)
            <tr>
                <td>{{ $item->name }}</td>
                <td>{{ $item->electricity }}</td>
                <td>{{ $item->address }}</td>
                <td>

                    @if ($item->images->isNotEmpty())
                        @php
                            $path = public_path($item->images->first()->media);
                            $path = str_replace('/', '\\', $path);
                        @endphp
                        <img width=50 height="50"
                            src="{{ $path }}" />
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
