<li class="nav-item">
    <a href="{{ route('home') }}" class="nav-link {{ Request::is('home') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Home</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('buildings.index') }}" class="nav-link {{ Request::is('buildings.index') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Buildings</p>
    </a>
</li>
