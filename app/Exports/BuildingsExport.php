<?php

namespace App\Exports;

use App\Models\Building;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithMapping;

class BuildingsExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */

    use Exportable;
    private $data;
    private $images;

    public function __construct()
    {
        $this->data = Building::all();
    }
    public function view(): View
    {
        return view('exports.building', [
            'buildings' => $this->data,
        ]);
    }

    // public function query()
    // {
    //     return Building::query();
    //     // return Invoice::query()->whereYear('created_at', $this->year);
    // }
    // public function collection()
    // {
    //     return Building::with('images')->query();
    // }

    // public function map($building): array
    // {
    //     return [
    //         $building->name,
    //         $building->address,
    //         $building->electricity,
    //         $building->images->first() ? $building->images->first()->media : '', // Assuming you have a 'profile_image_url' field in your 'users' table
    //     ];
    // }
    // public function headings(): array
    // {
    //     return [
    //         'Name',
    //         'Address',
    //         'Electricity',
    //         'Image',
    //     ];
    // }
}
