<?php

namespace App\Exports;

use App\Models\Building;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class BuildingExport implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
   use Exportable;
   private $buildings;
   public function __construct()
    {
        $this->buildings = Building::all();
    }

   public function view(): View
   {
       return view('exports.building', [
           'buildings' => $this->buildings,
       ]);
   }
}
