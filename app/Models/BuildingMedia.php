<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BuildingMedia extends Model
{
    use HasFactory;
    protected $fillable=['building_id','media','media_type'];
    protected $table='building_medias';
}
