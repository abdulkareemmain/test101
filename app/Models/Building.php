<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Building extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable=['name','address','electricity'];

    public function images(){
        return $this->hasMany(BuildingMedia::class,'building_id');
    }
}
